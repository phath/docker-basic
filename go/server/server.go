package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/mux"
)

func StaticWebPage(w http.ResponseWriter, r *http.Request) {
	html := `
	<!DOCTYPE html>
	<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
	<style>
	#snackbar {
	    visibility: hidden; /* Hidden by default. Visible on click */
	    min-width: 250px; /* Set a default minimum width */
	    margin-left: -125px; /* Divide value of min-width by 2 */
	    background-color: #333; /* Black background color */
	    color: #fff; /* White text color */
	    text-align: center; /* Centered text */
	    border-radius: 2px; /* Rounded borders */
	    padding: 16px; /* Padding */
	    position: fixed; /* Sit on top of the screen */
	    z-index: 1; /* Add a z-index if needed */
	    left: 50%; /* Center the snackbar */
	    bottom: 30px; /* 30px from the bottom */
	}
	
	/* Show the snackbar when clicking on a button (class added with JavaScript) */
	#snackbar.show {
	    visibility: visible; /* Show the snackbar */
	
	/* Add animation: Take 0.5 seconds to fade in and out the snackbar.
	However, delay the fade out process for 2.5 seconds */
	    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
	    animation: fadein 0.5s, fadeout 0.5s 2.5s;
	}
	
	/* Animations to fade the snackbar in and out */
	@-webkit-keyframes fadein {
	    from {bottom: 0; opacity: 0;}
	    to {bottom: 30px; opacity: 1;}
	}
	
	@keyframes fadein {
	    from {bottom: 0; opacity: 0;}
	    to {bottom: 30px; opacity: 1;}
	}
	
	@-webkit-keyframes fadeout {
	    from {bottom: 30px; opacity: 1;}
	    to {bottom: 0; opacity: 0;}
	}
	
	@keyframes fadeout {
	    from {bottom: 30px; opacity: 1;}
	    to {bottom: 0; opacity: 0;}
	}
		</style>
    </head>
	<html>
		<body>
			<h2>
				<center>
					IRIS Prediction
				</center>
			</h2>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<center>
				<table width="30%">
					<tr>
					<th>Sepal Length</th>
					<th>Sepal Width</th>
					<th>Petal Length</th>
					<th>Petal Width</th>
					</tr>
					<tr>
					<th><input id="sepal_length" type="text" value="5.8"></th>
					<th><input id="sepal_width" type="text" value="2.7"></th>
					<th><input id="petal_length" type="text" value="5.1"></th>
					<th><input id="petal_width" type="text" value="1.9"></th>
					</tr>
				</table>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
				<button id="predict-btn" type="submit" class="btn btn-primary">Predict</button>
						<script>
							$(document).ready(function() {
								$("#predict-btn").on('click', function() {
									var sl = document.getElementById("sepal_length");
									var sw = document.getElementById("sepal_width");
									var pl = document.getElementById("petal_length");
									var pw = document.getElementById("petal_width");
									$.ajax({
										method: "GET",
  	   									url: "/callML",
										data: {sl: sl.value, sw: sw.value, pl: pl.value, pw: pw.value},
										success: function (data) { showSnackbar(data)}
  									});
								});
							});
						</script>
			</center>
			<div id="snackbar">Done!</div>
			<script>
            	function showSnackbar(data) {
					$("div#snackbar").text(data);
                	var x = document.getElementById("snackbar");
                    x.className = "show";
                    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            	}
			</script>
		</body>
	</html>
	`
	buf := bytes.NewBuffer([]byte{})
	buf.WriteString(html)
	header := w.Header()
	header.Set("Content-Type", "text/html")
	w.WriteHeader(http.StatusOK)
	w.Write(buf.Bytes())
}

func CallMLPrediction(w http.ResponseWriter, r *http.Request) {
	mlhost := os.Getenv("ML_HOST")
	fmt.Println("call mlserver for prediction")
	fmt.Println(mlhost)
	sl := r.FormValue("sl")
	sw := r.FormValue("sw")
	pl := r.FormValue("pl")
	pw := r.FormValue("pw")

	url := mlhost + "/predict/" + sl + "/" + sw + "/" + pl + "/" + pw
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
		w.Write([]byte("Failed to contact ml server"))
		return
	}
	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	var dat map[string]interface{}
	err = json.Unmarshal(body, &dat)
	if err != nil {
		fmt.Println(err)
		w.Write([]byte("Failed to decode json"))
		return
	}
	w.Write([]byte("Belongs to class " + strconv.Itoa(int(dat["class"].(float64)))))

}

func main() {
	r := mux.NewRouter()
	// Routes consist of a path and a handler function.
	r.HandleFunc("/", StaticWebPage).Methods("GET")
	r.HandleFunc("/callML", CallMLPrediction).Methods("GET")

	// Bind to a port and pass our router in
	fmt.Println("server launching")
	log.Fatal(http.ListenAndServe(":8433", r))
}
