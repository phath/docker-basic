#The base image (this is a small image that contains python3)
FROM        python:3-alpine3.7

#Maintainer (not nescessarily)
MAINTAINER  me@xplore.vn

#Python enviroment dependencies
RUN        apk update
RUN        apk add --no-cache --update postgresql-dev
RUN        apk add --no-cache --update libstdc++
RUN        apk add --no-cache --update lapack-dev
RUN        apk add --no-cache --update libunwind-dev
RUN        apk add --no-cache freetype freetype-dev
RUN        apk add --no-cache \
                   --virtual=.build-deps \
                   g++ gfortran musl-dev \
                   python3-dev make git libexecinfo-dev && \
           ln -s locale.h /usr/include/xlocale.h

##Basic libs for a datascience task including: database query, data manipulation, machine learning, graphical lib
RUN        python3 -m pip install psycopg2 
RUN        python3 -m pip install numpy    
RUN        python3 -m pip install pandas
RUN        python3 -m pip install scipy
RUN        python3 -m pip install sklearn 
RUN        python3 -m pip install -U setuptools
RUN        python3 -m pip install matplotlib 
RUN        python3 -m pip install plotly
RUN        python3 -m pip install flask 
