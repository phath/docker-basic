import numpy as np
from sklearn.cluster import KMeans
from sklearn import datasets


def predictIris(a, b, c, d):
    np.random.seed(5)
    
    iris = datasets.load_iris()
    X = iris.data
    print(X)
    y = iris.target
    
    estimators = KMeans(n_clusters=3)
    estimators.fit(X)
    result = estimators.predict(np.array([[a,b,c,d]]))
    return result[0]

if __name__ == "__main__":
    a, b, c, d = (5.6, 2.7,  4.2,  1.3) 
    rs = predictIris(a, b, c, d)
    print(rs)
