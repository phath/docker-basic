from flask import Flask, jsonify
import numpy as np
from sklearn.cluster import KMeans
from sklearn import datasets

app = Flask(__name__)

def predictIris(a, b, c, d):
    np.random.seed(5)
    
    iris = datasets.load_iris()
    X = iris.data
    y = iris.target
    
    estimators = KMeans(n_clusters=3)
    estimators.fit(X)
    result = estimators.predict(np.array([[a,b,c,d]]))
    return result[0]

@app.route('/predict/<float:sepal_len>/<float:sepal_wth>/<float:petal_len>/<float:petal_wth>')
def getPrediction(sepal_len=0, sepal_wth=0, petal_len=0, petal_wth=0):
    rs = predictIris(sepal_len, sepal_wth, petal_len, petal_wth)
    return jsonify({"class": int(rs)})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8432)
